//1
function filterFalsyValues (a) {
    var filtered = [];
    for(var i=0; i < a.length; i++) {
        if (a[i] === 0 || a[i] === '' || a[i] !== a[i] || a[i] === false) {
            i++
        }
        if(typeof a[i] === "string" || typeof a[i] === "number" || typeof a[i] === "boolean") {
            filtered.push(a[i])
        }
    } return filtered
}
console.log(filterFalsyValues([0,1,2,3,true,false,null,undefined,NaN,'hello','',122,null,NaN,undefined,true]))

//2
function getMultipliedBigNumber(array) {
    var hugeNumbers = [];
    for(var i = 0; i < array.length; i++) {
        if(array[i]*array[i] > 1000) {
            var mul = array[i]*array[i]
            hugeNumbers.push(mul)
        }
    } return hugeNumbers

}
console.log(getMultipliedBigNumber([1,110,4,99,3,333,444,33,3,555,444,55,33]))
//3
function getElementsSum(a) {
    var sum = 0;
    for (var i=0; i < a.length; i++) {
        if (typeof a[i] === "number") {
            sum += a[i]
        } else if(typeof a[i] === "string") {
            sum += a[i].length
        }
    } return sum
}
console.log(getElementsSum([1,3,4,5,6,7,'hello','well','hello, i m here']))
//4
function reverseArray(a) {
    var reversed = [];
    for(var i = a.length; i--;) {
        reversed.push(a[i])
    } return reversed
}
console.log(reverseArray([1,3,4,5,6,7,8,9,10]))